module gitlab.cern.ch/linuxsupport/cernpkg

require (
	github.com/cavaliercoder/go-rpm v0.0.0-20180809195645-44d49fb2a883
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/mitchellh/go-homedir v1.0.0
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7 // indirect
	github.com/sassoftware/go-rpmutils v0.0.0-20180124145353-8c470ded63c3
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.2.0
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
	golang.org/x/crypto v0.0.0-20180910181607-0e37d006457b // indirect
)
